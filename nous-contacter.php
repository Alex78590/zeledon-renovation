<?php include_once 'gestion_Base.php'?>
<!DOCTYPE html>
<html lang="en">
<?php 
 $imageFond = getImageFondIndex();
 foreach($imageFond as $img):
?>

<?php endforeach?>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php 
 $nomSite = getNomSite();
foreach ($nomSite as $nomS):
?>
<?php echo $nomS['nomSite']?>
<?php endforeach;?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i" rel="stylesheet">

    <!-- Custom styles for this template -->
        <style>body{font-family:Lora;background:-webkit-gradient(linear,left top,left bottom,from(rgba(47,23,15,.65))),url("<?php echo substr ($img['imageFond'],3)?>");background:linear-gradient(rgba(47,23,15,.65)),url(img/chantier.jpg);background-attachment:fixed;background-position:center;background-size:cover}h1,h2,h3,h4,h5,h6{font-family:Raleway}p{line-height:1.75}.text-faded{color:rgba(255,255,255,.3)}.site-heading{margin-top:5rem;margin-bottom:5rem;text-transform:uppercase;line-height:1;font-family:Raleway}.site-heading .site-heading-upper{display:block;font-size:2rem;font-weight:800}.site-heading .site-heading-lower{font-size:5rem;font-weight:100;line-height:4rem}.page-section{margin-top:5rem;margin-bottom:5rem}.section-heading{text-transform:uppercase}.section-heading .section-heading-upper{display:block;font-size:1rem;font-weight:800}.section-heading .section-heading-lower{display:block;font-size:3rem;font-weight:100}.bg-faded{background-color:rgba(255,255,255,.85)}#mainNav{background-color:rgba(47,23,15,.9);font-family:Raleway}#mainNav .navbar-brand{color:#e6a756}#mainNav .navbar-nav .nav-item .nav-link{color:rgba(255,255,255,.7);font-weight:800}#mainNav .navbar-nav .nav-item.active .nav-link{color:#e6a756}@media (min-width:992px){#mainNav .navbar-nav .nav-item .nav-link{font-size:.9rem}#mainNav .navbar-nav .nav-item .nav-link:hover{color:rgba(255,255,255,.4)}#mainNav .navbar-nav .nav-item.active .nav-link:hover{color:#e6a756}}.btn-xl{font-weight:700;font-size:.8rem;padding-top:1.5rem;padding-bottom:1.5rem;padding-left:2rem;padding-right:2rem}.intro{position:relative}@media (min-width:992px){.intro .intro-img{width:75%;float:right}.intro .intro-text{left:0;width:60%;margin-top:3rem;position:absolute}.intro .intro-text .intro-button{width:100%;left:0;position:absolute;bottom:-2rem}}@media (min-width:1200px){.intro .intro-text{width:45%}}.cta{padding-top:5rem;padding-bottom:5rem;background-color:rgba(230,167,86,.9)}.cta .cta-inner{position:relative;padding:3rem;margin:.5rem;background-color:rgba(255,255,255,.85)}.cta .cta-inner:before{border-radius:.5rem;content:'';position:absolute;top:-.5rem;bottom:-.5rem;left:-.5rem;right:-.5rem;border:.25rem solid rgba(255,255,255,.85)}@media (min-width:992px){.about-heading .about-heading-img{position:relative;z-index:0}.about-heading .about-heading-content{margin-top:-5rem;position:relative;z-index:1}}@media (min-width:992px){.product-item .product-item-title{position:relative;z-index:1;margin-bottom:-3rem}.product-item .product-item-img{position:relative;z-index:0;max-width:60vw}.product-item .product-item-description{position:relative;z-index:1;margin-top:-3rem;max-width:50vw}}.list-hours{font-size:.9rem}.list-hours .list-hours-item{border-bottom:1px solid rgba(230,167,86,.5);padding-bottom:.25rem;margin-bottom:1rem;font-style:italic}.list-hours .list-hours-item.today{font-weight:700;color:#e6a756}@media (min-width:992px){.list-hours{width:50%;font-size:1.1rem}}.address strong{font-size:1.2rem}.footer{background-color:rgba(47,23,15,.9)}.text-primary{color:#e6a756!important}.bg-primary{background-color:#e6a756!important}.btn{-webkit-box-shadow:0 3px 3px 0 rgba(33,37,41,.1);box-shadow:0 3px 3px 0 rgba(33,37,41,.1)}.btn-primary{background-color:#e6a756;border-color:#e6a756}.btn-primary:active,.btn-primary:focus,.btn-primary:hover{background-color:#df902a;border-color:#df902a}.font-weight-light{font-weight:100!important}</style>


  </head>

  <body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3"><?php $titleMenu = getTitreSite();
        foreach ($titleMenu as $title):
            ?>
            <?php echo $title['titreSite']?></option>
            <?php endforeach;?></span>
      <?php
$titre = getTitreCt();
        foreach ($titre as $tit):
            ?>
<?php echo $tit['titre']?>
<?php endforeach;?></span>
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="index.php"><?php
$titreMenu = getTitreMenuIndex();
        foreach ($titreMenu as $titre):
            ?>
<?php echo $titre['titreMenu']?>
            <?php endforeach;?>
              </a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="nos-competences.php"><?php $titreMenu = getTitreMenuComp();
        foreach ($titreMenu as $titM):
            ?>
<?php echo $titM['titreMenu']?>
<?php endforeach;?></a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="nos-realisations.php"><?php $titreMenu = getTitreMenuRea();
        foreach ($titreMenu as $titM):
            ?>
<?php echo $titM['titreMenu']?>
<?php endforeach;?></a>
            </li>
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="notre-entreprise.php"><?php
$titreMenu = getTitreMenuEnt();
        foreach ($titreMenu as $titM):
            ?>
<?php echo $titM['titreMenu']?>
<?php endforeach;?></a>
            </li>
            <li class="nav-item active px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="nous-contacter.php"><?php
$titreMenu = getTitreMenuCt();
        foreach ($titreMenu as $titM):
            ?>
<?php echo $titM['titreMenu']?>
<?php endforeach;?> <span class="sr-only">(current)</span></a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <section class="page-section cta">
      
        <div class="row">
          <div class="col-xl-9 mx-auto">
          
            <center>
              
                <em><strong>
                <?php
$area1 = getArea1Ct();
        foreach ($area1 as $ar1):
            ?>
<?php echo $ar1['area1']?>
<?php endforeach;?></span>
                <span class="section-heading-lower"><?php
$area2 = getArea2Ct();
        foreach ($area2 as $ar2):
            ?>
<?php echo $ar2['area2']?>
<?php endforeach;?><br><br></strong>
                  <?php
$adresse = getAdresseCt();
        foreach ($adresse as $adr):
            ?>
<?php echo $adr['adresse']?>
<?php endforeach;?></strong>
                  <br>
                  <?php
$adresse1 = getAdresse1Ct();
        foreach ($adresse1 as $adr1):
            ?>
<?php echo $adr1['adresse1']?>
<?php endforeach;?>
                </em>
              </p>
              <p class="mb-0">
                <small>
                  <em><?php
$numero = getNumeroCt();
        foreach ($numero as $num):
            ?>
<?php echo $num['numero']?>
<?php endforeach;?></em>
                </small>
                <br>
                <?php
$numero1 = getNumero1Ct();
        foreach ($numero1 as $num1):
            ?>
<?php echo $num1['numero1']?>
<?php endforeach;?>
              </p>
              <p class="mb-0">
                <small>
                    <br>
                  <em><?php
$mail = getMailCt();
        foreach ($mail as $mel):
            ?>
<?php echo $mel['mail']?>
<?php endforeach;?></em>
                </small>
                <br>
                <?php
$mail1 = getMail1Ct();
        foreach ($mail1 as $mel1):
            ?><a href="mailto:
<?php echo $mel1['mail1']?>
<?php endforeach;?>"> <?php
$mail1 = getMail1Ct();
        foreach ($mail1 as $mel1):
            ?>
<?php echo $mel1['mail1']?>
<?php endforeach;?> </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    

    <footer class="footer text-faded text-center py-5">
      <div class="container">
        <p class="m-0 small"><?php 
 $copyright = getCopyright();
foreach ($copyright as $co):
?>
<?php echo $co['copyright']?>
<?php endforeach;?></p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

  

</html>
