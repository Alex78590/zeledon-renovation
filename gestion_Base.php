<?php session_start();
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//connexion
function gestionnaireDeConnexion() {
    $pdo = null;
    try {
        $pdo = new PDO(
                'mysql:host=localhost;dbname=site web', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    } catch (PDOException $err) {
        $messageErreur = $err->getMessage();
        error_log($messageErreur, 0);
    }
    return $pdo;
}
//fin connexion

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//create table
$pdo=gestionnaireDeConnexion();
    if  ($pdo != NULL){
        $sql = "CREATE TABLE IF NOT EXISTS competences(
            title1 VARCHAR(1000),
            titleText1 VARCHAR(3000),
            text1 VARCHAR(10000),
            title2 VARCHAR(1000),
            titleText2 VARCHAR(3000),
            text2 VARCHAR(10000),
            title3 VARCHAR(1000),
            titleText3 VARCHAR(3000),
            text3 VARCHAR(10000)
            ) ENGINE = INNODB;";
        $pdo->query($sql);
    }

    $pdo=gestionnaireDeConnexion();
    if  ($pdo != NULL){
        $sql = "CREATE TABLE IF NOT EXISTS realisations(
            img_id int AUTO_INCREMENT,
            img_nom varchar (100),
            primary key (img_id)
            ) ENGINE = INNODB;";
        $pdo->query($sql);
    }

    $pdo=gestionnaireDeConnexion();
    if  ($pdo != NULL){
        $sql = "CREATE TABLE IF NOT EXISTS menu(
            title1 VARCHAR(1000),
            titleText1 VARCHAR(3000),
            text1 VARCHAR(10000),
            title2 VARCHAR(1000),
            titleText2 VARCHAR(3000),
            text2 VARCHAR(10000),
            title3 VARCHAR(1000),
            titleText3 VARCHAR(3000),
            text3 VARCHAR(10000)
            ) ENGINE = INNODB;";
        $pdo->query($sql);
    }

    $pdo=gestionnaireDeConnexion();
    if  ($pdo != NULL){
        $sql = "CREATE TABLE IF NOT EXISTS entreprise(
            titleMenu VARCHAR(1000),
            titleText1 VARCHAR(3000),
            text1 VARCHAR(10000),
            title2 VARCHAR(1000),
            titleText2 VARCHAR(3000),
            text2 VARCHAR(10000),
            title3 VARCHAR(1000),
            titleText3 VARCHAR(3000),
            text3 VARCHAR(10000)
            ) ENGINE = INNODB;";
        $pdo->query($sql);
    }

    $pdo=gestionnaireDeConnexion();
    if  ($pdo != NULL){
        $sql = "CREATE TABLE IF NOT EXISTS contact(
            title1 VARCHAR(1000),
            titleText1 VARCHAR(3000),
            text1 VARCHAR(10000),
            title2 VARCHAR(1000),
            titleText2 VARCHAR(3000),
            text2 VARCHAR(10000),
            title3 VARCHAR(1000),
            titleText3 VARCHAR(3000),
            text3 VARCHAR(10000)
            ) ENGINE = INNODB;";
        $pdo->query($sql);
    }

    $pdo=gestionnaireDeConnexion();
    if  ($pdo != NULL){
        $sql = "CREATE TABLE IF NOT EXISTS admin(
            login varchar (100),
            mdp varchar (100)
            ) ENGINE = INNODB;";
        $pdo->query($sql);
    }
// fin create table

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//verification mdp et login
function verification ($login, $mdp)
{
    $_SESSION['login'] = false;
    $compteExistant = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != null)
    {
        $sql = "SELECT count(*) as nb FROM admin "." WHERE login = :login AND mdp = :mdp";
        $prep = $pdo -> prepare($sql);
        $prep -> bindParam (':login', $login, PDO::PARAM_STR);
        $prep -> bindParam (':mdp', $mdp, PDO::PARAM_STR);
        $prep -> execute();
        $resultat = $prep -> fetch();
        
        if ($resultat["nb"] == 1)
        {
            $compteExistant = true;
        }
        $prep ->closeCursor();
    }
    return $compteExistant;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titreSite
function getTitreSite()
{
    $titreSite = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT titreSite FROM menu ORDER BY titreSite DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreSite = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreSite;
}
//fin afficher titre site

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre site
function updateTitreSite() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreSite = $pdo->prepare("UPDATE menu SET titreSite = ?");
        $updateTitreSite->execute(array($_POST['titreSite']));
    }
}
//fin modifier titre site 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titre menu 
function getTitleMenu()
{
    $titleMenu = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT titleMenu FROM menu ORDER BY titleMenu DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titleMenu = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titleMenu;
}
//fin afficher titre menu

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre menu index
function updateTitleMenu() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitleMenu = $pdo->prepare("UPDATE menu SET titleMenu = ?");
        $updateTitleMenu->execute(array($_POST['titleMenu']));
    }
}
//fin modifier titre menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titre 
function getTitreMenuIndex()
{
    $titreMenuIndex = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT titreMenu FROM menu ORDER BY titreMenu DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreMenuIndex = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreMenuIndex;
}
//fin afficher titre 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre menu 
function updateTitreMenuIndex() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreMenuIndex = $pdo->prepare("UPDATE menu SET titreMenu = ?");
        $updateTitreMenuIndex->execute(array($_POST['titreMenu']));
    }
}
//fin modifier titre 

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area1 menu index
function getArea1Index()
{
    $area1Index = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT area1 FROM menu ORDER BY area1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area1Index = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area1Index;
}
//fin afficher area1 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area1 menu index
function updateArea1Index() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea1Index = $pdo->prepare("UPDATE menu SET area1 = ?");
        $updateArea1Index->execute(array($_POST['area1']));
    }
}
//fin modifier area1 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area2 menu index
function getArea2Index()
{
    $Area2Index = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT area2 FROM menu ORDER BY area2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $Area2Index = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $Area2Index;
}
//fin afficher area2 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area3 menu index
function getArea3Index()
{
    $area3Index = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT area3 FROM menu ORDER BY area3 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area3Index = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area3Index;
}
//fin afficher area3 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area3 menu index
function updateArea3Index() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea3Index = $pdo->prepare("UPDATE menu SET area3 = ?");
        $updateArea3Index->execute(array($_POST['area3']));
    }
}
//fin modifier area3 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area4 menu index
function getArea4Index()
{
    $area4Index = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $req = "SELECT area4 FROM menu ORDER BY area4 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area4Index = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area4Index;
}
//fin afficher area4 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area4 menu index
function updateArea4Index() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea4Index = $pdo->prepare("UPDATE menu SET area4 = ?");
        $updateArea4Index->execute(array($_POST['area4']));
    }
}
//fin modifier area4 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area2 menu index
function updateArea2Index() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea2Index = $pdo->prepare("UPDATE menu SET area2 = ?");
        $updateArea2Index->execute(array($_POST['area2']));
    }
}
//fin modifier area2 menu index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area1 competences
function getArea1Comp(){
    $area1Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area1 FROM competences ORDER BY area1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area1Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area1Comp;
}
//fin afficher area1 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area1 competences
function updateArea1Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea1Comp = $pdo->prepare("UPDATE competences SET area1 = ?");
        $updateArea1Comp->execute(array($_POST['area1']));
    }
}
//fin modifier area1 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area2 competences
function getArea2Comp(){
    $area1Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area2 FROM competences ORDER BY area2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area2Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area2Comp;
}
//fin afficher area2 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area2 competences
function updateArea2Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea2Comp = $pdo->prepare("UPDATE competences SET area2 = ?");
        $updateArea2Comp->execute(array($_POST['area2']));
    }
}
//fin modifier area2 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area3 competences
function getArea3Comp(){
    $area3Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area3 FROM competences ORDER BY area3 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area3Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area3Comp;
}
//fin afficher area3 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area3 competences
function updateArea3Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea3Comp = $pdo->prepare("UPDATE competences SET area3 = ?");
        $updateArea3Comp->execute(array($_POST['area3']));
    }
}
//fin modifier area3 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area4 competences
function getArea4Comp(){
    $area4Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area4 FROM competences ORDER BY area4 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area4Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area4Comp;
}
//fin afficher area4 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area4 competences
function updateArea4Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea4Comp = $pdo->prepare("UPDATE competences SET area4 = ?");
        $updateArea4Comp->execute(array($_POST['area4']));
    }
}
//fin modifier area4 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area5 competences
function getArea5Comp(){
    $area5Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area5 FROM competences ORDER BY area5 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area5Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area5Comp;
}
//fin afficher area5 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area5 competences
function updateArea5Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea5Comp = $pdo->prepare("UPDATE competences SET area5 = ?");
        $updateArea5Comp->execute(array($_POST['area5']));
    }
}
//fin modifier area5 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area6 competences
function getArea6Comp(){
    $area6Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area6 FROM competences ORDER BY area6 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area6Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area6Comp;
}
//fin afficher area6 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area6 competences
function updateArea6Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea6Comp = $pdo->prepare("UPDATE competences SET area6 = ?");
        $updateArea6Comp->execute(array($_POST['area6']));
    }
}
//fin modifier area6 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area7 competences
function getArea7Comp(){
    $area7Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area7 FROM competences ORDER BY area7 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area7Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area7Comp;
}
//fin afficher area7 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area7 competences
function updateArea7Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea7Comp = $pdo->prepare("UPDATE competences SET area7 = ?");
        $updateArea7Comp->execute(array($_POST['area7']));
    }
}
//fin modifier area7 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area8 competences
function getArea8Comp(){
    $area8Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area8 FROM competences ORDER BY area8 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area8Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area8Comp;
}
//fin afficher area8 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area8 competences
function updateArea8Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea8Comp = $pdo->prepare("UPDATE competences SET area8 = ?");
        $updateArea8Comp->execute(array($_POST['area8']));
    }
}
//fin modifier area8 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area9 competences
function getArea9Comp(){
    $area9Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area9 FROM competences ORDER BY area9 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area9Comp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area9Comp;
}
//fin afficher titre competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area9 competences
function updateArea9Comp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea9Comp = $pdo->prepare("UPDATE competences SET area9 = ?");
        $updateArea9Comp->execute(array($_POST['area9']));
    }
}
//fin modifier area9 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titre competences
function getTitreComp(){
    $titreComp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titre FROM competences ORDER BY titre DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreComp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreComp;
}
//fin afficher titre competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre competences
function updateTitreComp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreComp = $pdo->prepare("UPDATE competences SET titre = ?");
        $updateTitreComp->execute(array($_POST['titre']));
    }
}
//fin modifier titre competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titreMenu competences
function getTitreMenuComp(){
    $titreMenuComp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titreMenu FROM competences ORDER BY titreMenu DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreMenuComp = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreMenuComp;
}
//fin afficher titreMenu competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titreMenu competences
function updateTitreMenuComp(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreMenuComp = $pdo->prepare("UPDATE competences SET titreMenu = ?");
        $updateTitreMenuComp->execute(array($_POST['titreMenu']));
    }
}
//fin modifier titreMenu competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titreMenu entreprise
function getTitreMenuEnt(){
    $titreMenuEnt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titreMenu FROM entreprise ORDER BY titreMenu DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreMenuEnt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreMenuEnt;
}
//fin afficher titreMenu entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titreMenu entreprise
function updateTitreMenuEnt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreMenuEnt = $pdo->prepare("UPDATE entreprise SET titreMenu = ?");
        $updateTitreMenuEnt->execute(array($_POST['titreMenu']));
    }
}
//fin modifier titreMenu entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titre entreprise
function getTitreEnt(){
    $titreEnt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titre FROM entreprise ORDER BY titre DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreEnt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreEnt;
}
//fin afficher titre entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre entreprise
function updateTitreEnt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreEnt = $pdo->prepare("UPDATE entreprise SET titre = ?");
        $updateTitreEnt->execute(array($_POST['titre']));
    }
}
//fin modifier titre entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area1 entreprise
function getArea1Ent(){
    $area1Ent = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area1 FROM entreprise ORDER BY area1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area1Ent = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area1Ent;
}
//fin afficher area1 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area1 entreprise
function updateArea1Ent(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea1Ent = $pdo->prepare("UPDATE entreprise SET area1 = ?");
        $updateArea1Ent->execute(array($_POST['area1']));
    }
}
//fin modifier area1 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area2 entreprise
function getArea2Ent(){
    $area2Ent = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area2 FROM entreprise ORDER BY area2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area2Ent = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area2Ent;
}
//fin afficher area2 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area2 entreprise
function updateArea2Ent(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea2Ent = $pdo->prepare("UPDATE entreprise SET area2 = ?");
        $updateArea2Ent->execute(array($_POST['area2']));
    }
}
//fin modifier area2 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area3 entreprise
function getArea3Ent(){
    $area9Comp = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area3 FROM entreprise ORDER BY area3 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area3Ent = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area3Ent;
}
//fin afficher area3 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area3 entreprise
function updateArea3Ent(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea3Ent = $pdo->prepare("UPDATE entreprise SET area3 = ?");
        $updateArea3Ent->execute(array($_POST['area3']));
    }
}
//fin modifier area3 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area4 entreprise
function getArea4Ent(){
    $area4Ent = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area4 FROM entreprise ORDER BY area4 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area4Ent = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area4Ent;
}
//fin afficher area4 entreprise

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area4 entreprise
function updateArea4Ent(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea4Ent = $pdo->prepare("UPDATE entreprise SET area4 = ?");
        $updateArea4Ent->execute(array($_POST['area4']));
    }
}
//fin modifier area4 entreprise

////////////////////////////////////////youpi///////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titre contact
function getTitreCt(){
    $titreCt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titre FROM contact ORDER BY titre DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreCt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreCt;
}
//fin afficher titre contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre contact
function updateTitreCt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreCt = $pdo->prepare("UPDATE contact SET titre = ?");
        $updateTitreCt->execute(array($_POST['titre']));
    }
}
//fin modifier titre contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titreMenu contact
function getTitreMenuCt(){
    $titreMenuCt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titreMenu FROM contact ORDER BY titreMenu DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $TitreMenuCt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $TitreMenuCt;
}
//fin afficher titreMenu contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titreMenu contact
function updateTitreMenuCt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreMenuCt = $pdo->prepare("UPDATE contact SET titreMenu = ?");
        $updateTitreMenuCt->execute(array($_POST['titreMenu']));
    }
}
//fin modifier titreMenu contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area1 contact
function getArea1Ct(){
    $area1Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area1 FROM contact ORDER BY area1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area1Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area1Ct;
}
//fin afficher area1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area1 contact
function updateArea1Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea1Ct = $pdo->prepare("UPDATE contact SET area1 = ?");
        $updateArea1Ct->execute(array($_POST['area1']));
    }
}
//fin modifier area1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area2 contact
function getArea2Ct(){
    $area2Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area2 FROM contact ORDER BY area2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area2Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area2Ct;
}
//fin afficher area2 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area2 contact
function updateArea2Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea2Ct = $pdo->prepare("UPDATE contact SET area2 = ?");
        $updateArea2Ct->execute(array($_POST['area2']));
    }
}
//fin modifier area2 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour1 contact
function getJour1Ct(){
    $jour1Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour1 FROM contact ORDER BY jour1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour1Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour1Ct;
}
//fin afficher jour1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour1 contact
function updateJour1Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour1Ct = $pdo->prepare("UPDATE contact SET jour1 = ?");
        $updateJour1Ct->execute(array($_POST['jour1']));
    }
}
//fin modifier jour1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour2 contact
function getJour2Ct(){
    $jour2Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour2 FROM contact ORDER BY jour2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour2Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour2Ct;
}
//fin afficher jour2 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour2 contact
function updateJour2Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour2Ct = $pdo->prepare("UPDATE contact SET jour2 = ?");
        $updateJour2Ct->execute(array($_POST['jour2']));
    }
}
//fin modifier jour2 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour3 contact
function getJour3Ct(){
    $jour3Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour3 FROM contact ORDER BY jour3 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour3Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour3Ct;
}
//fin afficher jour3 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour3 contact
function updateJour3Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour3Ct = $pdo->prepare("UPDATE contact SET jour3 = ?");
        $updateJour3Ct->execute(array($_POST['jour3']));
    }
}
//fin modifier jour3 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour4 contact
function getJour4Ct(){
    $jour4Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour4 FROM contact ORDER BY jour4 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour4Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour4Ct;
}
//fin afficher jour4 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour4 contact
function updateJour4Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour4Ct = $pdo->prepare("UPDATE contact SET jour4 = ?");
        $updateJour4Ct->execute(array($_POST['jour4']));
    }
}
//fin modifier jour4 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour5 contact
function getJour5Ct(){
    $jour5Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour5 FROM contact ORDER BY jour5 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour5Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour5Ct;
}
//fin afficher jour5 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour5 contact
function updateJour5Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour5Ct = $pdo->prepare("UPDATE contact SET jour5 = ?");
        $updateJour5Ct->execute(array($_POST['jour5']));
    }
}
//fin modifier jour5 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour6 contact
function getJour6Ct(){
    $jour6Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour6 FROM contact ORDER BY jour6 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour6Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour6Ct;
}
//fin afficher jour6 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour6 contact
function updateJour6Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour6Ct = $pdo->prepare("UPDATE contact SET jour6 = ?");
        $updateJour6Ct->execute(array($_POST['jour6']));
    }
}
//fin modifier jour6 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher jour7 contact
function getJour7Ct(){
    $jour7Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT jour7 FROM contact ORDER BY jour7 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $jour7Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $jour7Ct;
}
//fin afficher jour7 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier jour7 contact
function updateJour7Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateJour7Ct = $pdo->prepare("UPDATE contact SET jour7 = ?");
        $updateJour7Ct->execute(array($_POST['jour7']));
    }
}
//fin modifier jour7 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire1 contact
function getHoraire1Ct(){
    $horaire1Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire1 FROM contact ORDER BY horaire1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire1Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire1Ct;
}
//fin afficher horaire1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire1 contact
function updateHoraire1Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire1Ct = $pdo->prepare("UPDATE contact SET horaire1 = ?");
        $updateHoraire1Ct->execute(array($_POST['horaire1']));
    }
}
//fin modifier horaire1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire2 contact
function getHoraire2Ct(){
    $horaire2Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire2 FROM contact ORDER BY horaire2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire2Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire2Ct;
}
//fin afficher horaire2 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire2 contact
function updateHoraire2Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire2Ct = $pdo->prepare("UPDATE contact SET horaire2 = ?");
        $updateHoraire2Ct->execute(array($_POST['horaire2']));
    }
}
//fin modifier horaire2 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire3 contact
function getHoraire3Ct(){
    $horaire3Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire3 FROM contact ORDER BY horaire3 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire3Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire3Ct;
}
//fin afficher horaire3 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire3 contact
function updateHoraire3Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire3Ct = $pdo->prepare("UPDATE contact SET horaire3 = ?");
        $updateHoraire3Ct->execute(array($_POST['horaire3']));
    }
}
//fin modifier horaire3 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire4 contact
function getHoraire4Ct(){
    $horaire4Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire4 FROM contact ORDER BY horaire4 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire4Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire4Ct;
}
//fin afficher horaire4 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire4 contact
function updateHoraire4Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire4Ct = $pdo->prepare("UPDATE contact SET horaire4 = ?");
        $updateHoraire4Ct->execute(array($_POST['horaire4']));
    }
}
//fin modifier horaire4 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire5 contact
function getHoraire5Ct(){
    $horaire5Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire5 FROM contact ORDER BY horaire5 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire5Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire5Ct;
}
//fin afficher horaire5 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire5 contact
function updateHoraire5Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire5Ct = $pdo->prepare("UPDATE contact SET horaire5 = ?");
        $updateHoraire5Ct->execute(array($_POST['horaire5']));
    }
}
//fin modifier horaire5 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire6 contact
function getHoraire6Ct(){
    $horaire6Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire6 FROM contact ORDER BY horaire6 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire6Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire6Ct;
}
//fin afficher horaire6 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire6 contact
function updateHoraire6Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire6Ct = $pdo->prepare("UPDATE contact SET horaire6 = ?");
        $updateHoraire6Ct->execute(array($_POST['horaire6']));
    }
}
//fin modifier horaire6 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher horaire7 contact
function getHoraire7Ct(){
    $horaire7Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT horaire7 FROM contact ORDER BY horaire7 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $horaire7Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $horaire7Ct;
}
//fin afficher horaire7 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier horaire7 contact
function updateHoraire7Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateHoraire7Ct = $pdo->prepare("UPDATE contact SET horaire7 = ?");
        $updateHoraire7Ct->execute(array($_POST['horaire7']));
    }
}
//fin modifier horaire7 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher adresse contact
function getAdresseCt(){
    $adresseCt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT adresse FROM contact ORDER BY adresse DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $adresseCt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $adresseCt;
}
//fin afficher adresse contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier adresse contact
function updateAdresseCt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateAdresseCt = $pdo->prepare("UPDATE contact SET adresse = ?");
        $updateAdresseCt->execute(array($_POST['adresse']));
    }
}
//fin modifier adresse contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher adresse1 contact
function getAdresse1Ct(){
    $adresse1Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT adresse1 FROM contact ORDER BY adresse1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $adresse1Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $adresse1Ct;
}
//fin afficher adresse1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier adresse1 contact
function updateAdresse1Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateAdresse1Ct = $pdo->prepare("UPDATE contact SET adresse1 = ?");
        $updateAdresse1Ct->execute(array($_POST['adresse1']));
    }
}
//fin modifier adresse1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher numero contact
function getNumeroCt(){
    $numeroCt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT numero FROM contact ORDER BY numero DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $numeroCt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $numeroCt;
}
//fin afficher numero contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier numero contact
function updateNumeroCt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateNumeroCt = $pdo->prepare("UPDATE contact SET numero = ?");
        $updateNumeroCt->execute(array($_POST['numero']));
    }
}
//fin modifier numero contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher numero1 contact
function getNumero1Ct(){
    $numero1Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT numero1 FROM contact ORDER BY numero1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $numero1Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $numero1Ct;
}
//fin afficher numero1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function updateNumero1Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateNumero1Ct = $pdo->prepare("UPDATE contact SET numero1 = ?");
        $updateNumero1Ct->execute(array($_POST['numero1']));
    }
}
//fin modifier numero1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher mail contact
function getMailCt(){
    $mailCt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT mail FROM contact ORDER BY mail DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $mailCt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $mailCt;
}
//fin afficher mail contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier mail contact
function updateMailCt(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateMailCt = $pdo->prepare("UPDATE contact SET mail = ?");
        $updateMailCt->execute(array($_POST['mail']));
    }
}
//fin modifier mail contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher mail1 contact
function getMail1Ct(){
    $mail1Ct = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT mail1 FROM contact ORDER BY mail1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $mail1Ct = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $mail1Ct;
}
//fin afficher mail1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier mail1 contact
function updateMail1Ct(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateMail1Ct = $pdo->prepare("UPDATE contact SET mail1 = ?");
        $updateMail1Ct->execute(array($_POST['mail1']));
    }
}
//fin modifier mail1 contact

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher image 1 avant
function getImage(){
    $image = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin FROM realisations ORDER BY image_chemin DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image;
}

//fin afficher image 1 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 1 avant

function ajout_image($chemin,$nom){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin = '$chemin', img_nom = '$nom' ");
        $resultat->execute(array($_POST['nom']));
        
    }
}
//fin upload image 1 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher image 2 avant
function getImage2(){
    $image2 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin2 FROM realisations ORDER BY image_chemin2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image2 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image2;
}

//fin afficher image 2 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 2 avant

function ajout_image2($chemin2,$nom2){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin2 = '$chemin2', img_nom2 = '$nom2' ");
        $resultat->execute(array($_POST['nom2']));
        
    }
}
//fin upload image 2 avant


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 3 avant

function ajout_image3($chemin3,$nom3){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin3 = '$chemin3', img_nom3 = '$nom3' ");
        $resultat->execute(array($_POST['nom3']));
        
    }
}
//fin upload image 3 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 3 avant
function getImage3(){
    $image3 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin3 FROM realisations ORDER BY image_chemin3 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image3 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image3;
}

//fin afficher image 3 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 4 avant

function ajout_image4($chemin4,$nom4){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin4 = '$chemin4', img_nom4 = '$nom4' ");
        $resultat->execute(array($_POST['nom4']));
        
    }
}
//fin upload image 4 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 4 avant
function getImage4(){
    $image4 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin4 FROM realisations ORDER BY image_chemin4 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image4 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image4;
}

//fin afficher image 4 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 5 avant

function ajout_image5($chemin5,$nom5){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin5 = '$chemin5', img_nom5 = '$nom5' ");
        $resultat->execute(array($_POST['nom5']));
        
    }
}
//fin upload image 5 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 5 avant
function getImage5(){
    $image5 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin5 FROM realisations ORDER BY image_chemin5 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image5 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image5;
}

//fin afficher image 5 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 6 avant

function ajout_image6($chemin6,$nom6){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin6 = '$chemin6', img_nom6 = '$nom6' ");
        $resultat->execute(array($_POST['nom6']));
        
    }
}
//fin upload image 6 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 6 avant
function getImage6(){
    $image6 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin6 FROM realisations ORDER BY image_chemin6 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image6 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image6;
}

//fin afficher image 6 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 7 avant

function ajout_image7($chemin7,$nom7){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin7 = '$chemin7', img_nom7 = '$nom7' ");
        $resultat->execute(array($_POST['nom7']));
        
    }
}
//fin upload image 7 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 7 avant
function getImage7(){
    $image7 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin7 FROM realisations ORDER BY image_chemin7 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image7 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image7;
}

//fin afficher image 7 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 8 avant

function ajout_image8($chemin8,$nom8){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin8 = '$chemin8', img_nom8 = '$nom8' ");
        $resultat->execute(array($_POST['nom8']));
        
    }
}
//fin upload image 8 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 8 avant
function getImage8(){
    $image8 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin8 FROM realisations ORDER BY image_chemin8 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image8 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image8;
}

//fin afficher image 8 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 9 avant

function ajout_image9($chemin9,$nom9){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin9 = '$chemin9', img_nom9 = '$nom9' ");
        $resultat->execute(array($_POST['nom9']));
        
    }
}
//fin upload image 9 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 9 avant
function getImage9(){
    $image9 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin9 FROM realisations ORDER BY image_chemin9 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image9 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image9;
}

//fin afficher image 9 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 10 avant

function ajout_image10($chemin10,$nom10){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE realisations set image_chemin10 = '$chemin10', img_nom10 = '$nom10' ");
        $resultat->execute(array($_POST['nom10']));
        
    }
}
//fin upload image 10 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher image 10 avant
function getImage10(){
    $image10 = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image_chemin10 FROM realisations ORDER BY image_chemin10 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image10 = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image10;
}

//fin afficher image 10 avant

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titre realisations
function getTitreRea(){
    $titreRea = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titre FROM realisations ORDER BY titre DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $titreRea = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $titreRea;
}
//fin afficher titre realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titre realisations
function updateTitreRea(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreRea = $pdo->prepare("UPDATE realisations SET titre = ?");
        $updateTitreRea->execute(array($_POST['titre']));
    }
}
//fin modifier titre realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher titreMenu realisations
function getTitreMenuRea(){
    $titreMenuRea = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT titreMenu FROM realisations ORDER BY titreMenu DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $TitreMenuRea = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $TitreMenuRea;
}
//fin afficher titreMenu realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier titreMenu realisations
function updateTitreMenuRea(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateTitreMenuRea = $pdo->prepare("UPDATE realisations SET titreMenu = ?");
        $updateTitreMenuRea->execute(array($_POST['titreMenu']));
    }
}
//fin modifier titreMenu realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area1 realisations
function getArea1Rea(){
    $area1Rea = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area1 FROM realisations ORDER BY area1 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area1Rea = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area1Rea;
}
//fin afficher area1 realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area1 realisations
function updateArea1Rea(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea1Rea = $pdo->prepare("UPDATE realisations SET area1 = ?");
        $updateArea1Rea->execute(array($_POST['area1']));
    }
}
//fin modifier area1 realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher area2 contact
function getArea2Rea(){
    $area2Rea = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT area2 FROM realisations ORDER BY area2 DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $area2Rea = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $area2Rea;
}
//fin afficher area2 realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier area2 realisations
function updateArea2Rea(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateArea2Rea = $pdo->prepare("UPDATE realisations SET area2 = ?");
        $updateArea2Rea->execute(array($_POST['area2']));
    }
}
//fin modifier area2 realisations

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image fond index
function getImageFondIndex(){
    $imageFond = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT imageFond FROM menu ORDER BY imageFond DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $imageFond = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $imageFond;
}

//fin afficher image fond index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image fond index

function ajout_imageFondIndex($cheminImageFond,$nomImage){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE menu set imageFond = '$cheminImageFond', nomImage = '$nomImage' ");
        $resultat->execute(array($_POST['nomImage']));
        
    }
}
//fin upload image fond index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image principale index
function getImagePrincipaleIndex(){
    $imagePrincipale = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT imagePrincipale FROM menu ORDER BY imagePrincipale DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $imagePrincipale = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $imagePrincipale;
}

//fin afficher image principale index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image principale index

function ajout_imagePrincipaleIndex($cheminImagePrincipale,$nomImagePrincipale){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE menu set imagePrincipale = '$cheminImagePrincipale', nomImagePrincipale = '$nomImagePrincipale' ");
        $resultat->execute(array($_POST['nomImagePrincipale']));
        
    }
}
//fin upload image principale index

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 1 competences
function getImage1Competences(){
    $image1Competences = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image1Competences FROM competences ORDER BY image1Competences DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image1Competences = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image1Competences;
}

//fin afficher 1 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 1 competences

function ajout_image1Competences($cheminImage1Competences,$nomImage1Competences){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE competences set image1Competences = '$cheminImage1Competences', nomImage1Competences = '$nomImage1Competences' ");
        $resultat->execute(array($_POST['nomImage1Competences']));
        
    }
}
//fin upload image 1 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 2 competences
function getImage2Competences(){
    $image2Competences = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image2Competences FROM competences ORDER BY image2Competences DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image2Competences = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image2Competences;
}

//fin afficher 2 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 2 competences

function ajout_image2Competences($cheminImage2Competences,$nomImage2Competences){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE competences set image2Competences = '$cheminImage2Competences', nomImage2Competences = '$nomImage2Competences' ");
        $resultat->execute(array($_POST['nomImage2Competences']));
        
    }
}
//fin upload image 2 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//afficher image 3 competences
function getImage3Competences(){
    $image3Competences = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image3Competences FROM competences ORDER BY image3Competences DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $image3Competences = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $image3Competences;
}

//fin afficher 3 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 3 competences

function ajout_image3Competences($cheminImage3Competences,$nomImage3Competences){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE competences set image3Competences = '$cheminImage3Competences', nomImage3Competences = '$nomImage3Competences' ");
        $resultat->execute(array($_POST['nomImage3Competences']));
        
    }
}
//fin upload image 3 competences

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher nom du site
function getNomSite(){
    $nomSite = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT nomSite FROM menu ORDER BY nomSite DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $nomSite = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $nomSite;
}
//fin afficher nom du site

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier nom du site
function updateNomSite(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateNomSite = $pdo->prepare("UPDATE menu SET nomSite = ?");
        $updateNomSite->execute(array($_POST['nomSite']));
    }
}
//fin modifier nom du site

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher copyright site
function getCopyright(){
    $copyright = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT copyright FROM menu ORDER BY copyright DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $copyright = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $copyright;
}
//fin afficher copyright site

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier copyright site
function updateCopyright(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateCopyright = $pdo->prepare("UPDATE menu SET copyright = ?");
        $updateCopyright->execute(array($_POST['copyright']));
    }
}
//fin modifier copyright site

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher vitesse défilement image
function getVitesse(){
    $vitesse = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT vitesse FROM realisations ORDER BY vitesse DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $vitesse = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $vitesse;
}
//fin afficher vitesse défilement image

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//modifier vitesse défilement image
function updateVitesse(){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $updateVitesse = $pdo->prepare("UPDATE realisations SET vitesse = ?");
        $updateVitesse->execute(array($_POST['vitesse']));
    }
}
//fin modifier vitesse défilement image

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//afficher image 1 ent
function getImageEnt(){
    $imageEnt = array();
    $pdo = gestionnaireDeConnexion();
    if ($pdo!=NULL){
        $req = "SELECT image FROM entreprise ORDER BY image DESC LIMIT 1";
        $pdoStatement = $pdo->query($req);
        $imageEnt = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }
    return $imageEnt;
}

//fin afficher image 1 ent

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//upload image 1 ent

function ajout_imageEnt($chemin,$nom){
    $pdo = gestionnaireDeConnexion();
    if ($pdo != NULL) {
        $resultat = $pdo->prepare("UPDATE entreprise set image = '$chemin', image_nom = '$nom' ");
        $resultat->execute(array($_POST['image_nom']));
        
    }
}
//fin upload image 1 ent
?>