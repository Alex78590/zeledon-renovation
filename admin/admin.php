<?php session_start();?>
<!DOCTYPE html>
<link href="style.admin.css" rel="stylesheet">
<html>
    <head>
        <title>Page d'administration du site</title>
        <link rel="stylesheet" type="text/css" href="../style/style.css" />
        <meta charset="utf-8" />
    </head>
   
    <body>
    <?php if (empty($_SESSION["login"])): ?>
      <div class="signup-form">
        <form action="admin.co.traitement.php" method="post">
            <h1> Connection </h1>
            <input type="text" placeholder="login" name="login" id="login" class="txtb">
            <input type="password" placeholder="mdp" name="mdp" id="mdp" class="txtb">
            <input type="submit" value="Valider" class="signup-btn">
            <br>
            
            <p><a href="../">Retour au site</a></p>
        </form>
      </div>

      <?php else: ?>
       
            <p>Vous etes bien connecté avec l'utilisateur '<?php echo $_SESSION['login'] ?>'</p>
        <p><a href="menu.admin.php">ADMINISTRER LE SITE</a></p>
        <p><a href="admin.co.traitement.php?logout=1" onclick="return(confirm('Etes-vous sûr de vouloir vous déconnecter?'));">DECONNEXION</a></p>
        <?php endif; ?>
        </form>
    </body>
</html>

