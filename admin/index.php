<form method="POST" action="index.php" enctype="multipart/form-data">
<?php
  // Create database connection
  $db = mysqli_connect("localhost", "root", "", "site web");

  // Initialize message variable
  $msg = "";

  // If upload button is clicked ...
  if (isset($_POST['upload'])) {
  	// Get image name
  	$image_chemin = $_FILES['image']['name'];
  	// Get text
  	$img_nom = mysqli_real_escape_string($db, $_POST['img_nom']);

  	// image file directory
	  $target = "../img/".basename($image_chemin);
	  
  	$sql = "INSERT INTO realisations (image_chemin, img_nom) VALUES ('$image_chemin', '$img_nom')";
  	// execute query
  	mysqli_query($db, $sql);

  	if (move_uploaded_file($_FILES['image']['tmp_name'], $target)) {
  		$msg = "Image uploaded successfully";
  	}else{
  		$msg = "Failed to upload image";
  	}
  }
  $result = mysqli_query($db, "SELECT * FROM realisations");
?>
<!DOCTYPE html>
<html>
<head>
<title>Image Upload</title>
<style type="text/css">
   #content{
   	width: 50%;
   	margin: 20px auto;
   	border: 1px solid #cbcbcb;
   }
   form{
   	width: 50%;
   	margin: 20px auto;
   }
   form div{
   	margin-top: 5px;
   }
   #img_div{
   	width: 80%;
   	padding: 5px;
   	margin: 15px auto;
   	border: 1px solid #cbcbcb;
   }
   #img_div:after{
   	content: "";
   	display: block;
   	clear: both;
   }
   img{
   	float: left;
   	margin: 5px;
   	width: 300px;
   	height: 140px;
   }
</style>
</head>
<body>
<div id="content">
  <?php
    while ($row = mysqli_fetch_array($result)) {
	  echo "<div id='img_div'>";
	  echo "<form action='delete.php' method='POST'";
      	echo "<img src='../img/".$row['image_chemin']."' ><input type='submit' name='delete' value='supprimer' id='delete'>";
		  echo "<p>".$row['img_nom']."</p>";
		  echo "</form>";
      echo "</div>";
    }
  ?>
  <form method="POST" action="index.php" enctype="multipart/form-data">
  	<input type="hidden" name="size" value="1000000">
  	<div>
  	  <input type="file" name="image">
  	</div>
  	<div>
      <textarea 
      	id="text" 
      	cols="40" 
      	rows="4" 
      	name="img_nom"
      	placeholder="Say something about this image..."></textarea>
  	</div>
  	<div>
  		<button type="submit" name="upload">POST</button>
  	</div>
  </form>
</div>
</body>
</html>