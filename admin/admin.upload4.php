<?php 
include_once '../gestion_Base.php';
$dossier = '../img/';
$fichier = basename($_FILES['avatar4']['name']);
$extensions = array('.png', '.gif', '.jpg', '.jpeg');
$extension = strrchr($_FILES['avatar4']['name'], '.');
$nom=$_POST['nom4'];


 
		//Début des vérifications de sécurité...
		if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
		{
			$erreur = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg...';
			echo "<a href='admin.realisations.php'?'><br><br>RETOUR</a>";
		}
		
		if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
		{
			//formatage du nom (suppression des accents, remplacements des espaces par "-")
			
			if(move_uploaded_file($_FILES['avatar4']['tmp_name'], $dossier . $fichier)) //correct si la fonction renvoie TRUE
			{
				echo 'Upload effectué avec succès ! ';
                $chemin = $dossier . $fichier;
				ajout_image4($chemin,$nom);
				echo  "<a href='admin.realisations.php'?'><br><br>RETOUR</a>";
			}
			else //sinon, cas où la fonction renvoie FALSE
			{
                echo 'Echec de l\'upload de : ';
                echo $fichier ;
                echo ' ';
                echo 'dans : ';
				echo $dossier;
				echo "<a href='admin.realisations.php'?'><br><br>RETOUR</a>"; 
		    }
		}
		else
		{
			echo $erreur;
        }
        ?>